require("dotenv").config();
const restify = require("restify");
const errors = require("restify-errors");
const fashionlunch = require("fashionlunch-parser");
const moment = require("moment");

const SlackWebClient = require("@slack/client").WebClient;

const slackBotToken = process.env.SLACK_BOT_API_TOKEN;
const slackWebClient = new SlackWebClient(slackBotToken);

const server = restify.createServer({
    version: "1.0.0",
});

const PORT = parseInt(process.env.PORT) || 5000;

String.prototype.upperCaseFirst = function() {
    return this.charAt(0).toUpperCase() + this.substr(1);
}

server.use(restify.plugins.bodyParser({
    maxBodySize: 0,
    mapParams: true,
    mapFiles: false,
    overrideParams: false,
    rejectUnknown: true,
}));
server.use(restify.plugins.throttle({
    burst: 10,
    rate: 5,
    ip: true,
}));

server.use((req, res, next) => {
    if (!req.hasOwnProperty("body")) {
        return next(new errors.BadRequestError());
    }

    if (!req.body.hasOwnProperty("type")) {
        return next(new errors.BadRequestError());
    }

    if (!req.body.hasOwnProperty("token")) {
        return next(new errors.UnauthorizedError("Missing token"));
    } else if (req.body.token !== process.env.SLACK_REQUEST_VERIFICATION_TOKEN) {
        return next(new errors.UnauthorizedError("Token is invalid"));
    }

    return next();
});

const workDays = [
    "monday",
    "tuesday",
    "wednesday",
    "thursday",
    "friday",
];

server.post({
    path: "/lunchbot",
    version: [
        "1.0.0",
    ],
}, (req, res, next) => {
    if (req.body.hasOwnProperty("challenge") && req.body.type === "url_verification") {
        return res.send({
            challenge: req.body.challenge,
        });
    }

    res.send({
        ok: true
    });

    if (req.body.event.user === process.env.SLACK_BOT_ID) {
        return;
    }

    if (
        req.body.event.type !== "message" &&
        !req.body.event.hasOwnProperty("subtype") &&
        !req.body.event.text.includes(`<@${process.env.SLACK_BOT_ID}>`)
    ) {
        return;
    }

    const dayMoment = moment(req.body.event_time, "X");
    // const day = dayMoment.format("dddd");
    const day = "friday";
    const theDayString = dayMoment.locale("sv").format("dddd").upperCaseFirst() + "ens";

    if (!workDays.includes(day)) {
        slackWebClient.chat.postMessage(req.body.event.channel, "Det är helg... :zzz:", {
            as_user: true,
        }, (error, response) => {
            if (error) {
                console.log(`Error: ${error}`);
            } else {
                console.log(`Message sent: ${JSON.stringify(response, null, 2)}}`);
            }
        });
        return;
    }

    fashionlunch().then((menu) => {
        if (!menu.hasOwnProperty(day)) {
            slackWebClient.chat.postMessage(req.body.event.channel, "Det saknas en meny idag. :cry:", {
                as_user: true,
            }, (error, response) => {
                if (error) {
                    console.log(`Error: ${error}`);
                } else {
                    console.log(`Message sent: ${JSON.stringify(response, null, 2)}`);
                }
            });
        }

        const todaysMenu = menu[day]

        let result = {
            attachments: [
                {
                    fallback: `${theDayString} meny hos Lunchmästar'n`,
                    color: "default",
                    title: `${theDayString} meny hos Lunchmästar'n`,
                    title_link: "http://www.kvartersmenyn.se/rest/13537",
                    fields: [],
                    footer: "Kvartersmenyn",
                    footer_icon: "https://i.imgur.com/sMguSOZ.png",
                },
            ],
        };

        todaysMenu.meals.forEach((meal, index) => {
            result.attachments[0].fields.push({
                value: meal,
                short: false,
            });
        });

        slackWebClient.chat.postMessage(req.body.event.channel, "", {
            as_user: true,
            attachments: result.attachments,
        }, (error, response) => {
            if (error) {
                console.log(`Error: ${error}`);
            } else {
                console.log(`Message sent: ${JSON.stringify(response, null, 2)}}`);
            }
        });
    }).catch((error) => {
        console.error(error);

        slackWebClient.chat.postMessage(req.body.event.channel, "Ledsen, något gick fel!", {
            as_user: true,
            attachments: result.attachments,
        }, (error, response) => {
            if (error) {
                console.log(`Error: ${error}`);
            } else {
                console.log(`Message sent: ${JSON.stringify(response, null, 2)}}`);
            }
        });
    });
});

server.listen(PORT, () => {
    console.log(`Listening on *:${PORT}`);
});